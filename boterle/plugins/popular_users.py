from settings import POPULAR_USERS

from .base import BasePlugin


class OnMessagePopUsers(BasePlugin):
    async def on_message(self, message):
        for user in POPULAR_USERS:
            p_type = user['TYPE']
            p_glob = user['GLOB']
            p_var_name = user['VAR_NAME']
            p_message = user['MESSAGE']
            p_success = user.get('SUCCESS', 50)
            p_reset = user.get('RESET', 50)
            if p_type == 'speaking':
                if any(pattern in message.author.name.lower()
                       for pattern in p_glob):
                    await self.counter(p_var_name)
                    if getattr(self, p_var_name) >= p_success:
                        setattr(self, p_var_name, p_reset)
                        await self.bot.send_message(message.channel, p_message)
            elif p_type == 'mentioned':
                if any(pattern in message.content.lower()
                       for pattern in p_glob):
                    await self.counter(p_var_name)
                    if getattr(self, p_var_name) >= p_success:
                        setattr(self, p_var_name, p_reset)
                        await self.bot.send_message(message.channel, p_message)

    async def counter(self, attribute_name):
        if hasattr(self, attribute_name):
            current_value = int(getattr(self, attribute_name)) + 1
            setattr(self, attribute_name, current_value)
        else:
            setattr(self, attribute_name, 1)


def setup(bot):
    bot.add_cog(OnMessagePopUsers(bot))
